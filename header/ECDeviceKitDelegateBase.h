//
//  ECKitDelegateBase
//  CCPiPhoneSDK
//
//  Created by jiazy on 14/11/13.
//  Copyright (c) 2014年 ronglian. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "ECError.h"
#import "ECEnumDefs.h"
#import "ECMessage.h"

@protocol ChatSelectMemberManagerDelegate <NSObject>

/*
 选择了一组人
 */
- (void)ChatSelectMemberManager_DidSelectMembers:(NSArray*)aMemberArray type:(SelectObjectType)aType identifier:(NSString*)aIdentifier;

/*
 只选择了一个人
 */
- (void)ChatSelectMemberManager_DidSelectMember:(NSDictionary*)aMemberInformation type:(SelectObjectType)aType  identifier:(NSString*)aIdentifier;

/*
 只选择了一个群组（这个群组可能是是刚创建的，也可能是已经存在的）
 */
- (void)ChatSelectMemberManager_DidSelectGroup:(NSDictionary*)aGroupInformation type:(SelectObjectType)aType  identifier:(NSString*)aIdentifier;

@end

/**
 * 该代理用于接收登录和注销状态
 */
@protocol ECDeviceKitDelegateBase <NSObject>

@optional
-(NSArray*)onGoDialingView;
-(NSDictionary*)onGetUserInfo;

-(UIViewController *)getToMemberInfoViewWithID:(NSString *)memberId companyId:(NSString *)companyId;
/**
 获取选择联系人页面
 */
-(UIViewController *)getSelectMembersVCWithExceptMembers:(NSArray *)members WithSelectObjectType:(SelectObjectType)aPickType delegate:(id<ChatSelectMemberManagerDelegate>)aDelegate identifier:(NSString *)aIdentifier;
/**
 搜索用户（从 nickname、account、member_name、name_quanpin、name_initial、email、mobile进行模糊查找）
 */
- (NSMutableArray*)DBQuery_User_SearchUsersWithKeywords:(NSString*)aKeywords containOtherCompanyMember:(BOOL)containOtherCompanyMember;
- (NSMutableDictionary*)DBQuery_User_WithMemberId:(NSString*)aMemberId;
- (NSDictionary *)getLoginUserDic;
@required


@end
